import osmnx as ox
import networkx as nx
from sklearn.neighbors import KDTree
import folium
import matplotlib.pyplot as plt
from IPython.display import IFrame
ox.config(log_console=True, use_cache=True)

#wczytanie danych wejściowych
maxDistance =20000

start = (50.090415, 19.983268)
destination = (50.043625, 19.941000)

placesToVisit = []

#wczytanie miejscowości docelowych z pliku
f = open("miejsca", "r")
for x in f:
  print(x)
  x = x.strip()
  placesToVisit.append(x)

print(placesToVisit)

#Ustalenie sposobu sortowania
xDiff=abs(start[0]-destination[0])
yDiff=abs(start[1]-destination[1])
reverse = True
if xDiff>yDiff:

    if(start[0]-destination[0]> 0): reverse=True
    else: reverse=False
else:
    if (start[1] - destination[1] > 0):
        reverse = True
    else:
        reverse = False

#sortowanie miejscowości dodatkowych
placesToVisit.sort(key=lambda tup: tup[0 if xDiff > yDiff else 1], reverse = reverse)


start=ox.geocode(str(start[0]) +','+ str(start[1]))
destination=ox.geocode(str(destination[0]) +','+ str(destination[1]))
for  i in range(len(placesToVisit)):
  #place=ox.geocode(str(place[0]) +','+ str(place[1]))
  placesToVisit[i] = ox.geocode(placesToVisit[i])



G = ox.graph_from_point(start, distance=maxDistance, network_type='drive')
nodes, _ = ox.graph_to_gdfs(G)
nodes.head()
tree = KDTree(nodes[['y', 'x']], metric='euclidean')

#szukanie najbliższych węzłów wprowadzonych punktów
start_idx = tree.query([start], k=1, return_distance=False)[0]
destination_idx = tree.query([destination], k=1, return_distance=False)[0]

closest_node_to_placesToVisit = []
for i in range(len(placesToVisit)):
    placeToVisit_idx=tree.query([placesToVisit[i]], k=1, return_distance=False)[0]
    closest_node_to_placesToVisit.append(nodes.iloc[placeToVisit_idx].index.values[0])


closest_node_to_start = nodes.iloc[start_idx].index.values[0]
closest_node_to_destination = nodes.iloc[destination_idx].index.values[0]

#Wyznaczanie trasy, tak aby spełnić ograniczenia użytkownika na długość całej trasy
route = []
distance = 0
lastPoint = closest_node_to_start
lastPointNum = 0
finished = False
#dlugosc trasy z punktu startowego do docelowego
routeDirect = nx.shortest_path_length(G, closest_node_to_start, closest_node_to_destination, weight="length")
if routeDirect > maxDistance:
    print("Trasa od punktu startowego do docelowego jest dłuższa niż preferowana długość trasy")


#poniżej połączenie start z ktoryms z miejsc do odwiedzenia
for j in range(len(placesToVisit)):
    try:
        route_next = nx.shortest_path(G, closest_node_to_start, closest_node_to_placesToVisit[j])
    except nx.NetworkXNoPath:
        continue
    dist = nx.shortest_path_length(G, closest_node_to_start, closest_node_to_placesToVisit[j], weight="length")


    #lastPointNum = j
    if dist < maxDistance:
        if dist + nx.shortest_path_length(G, closest_node_to_placesToVisit[j], closest_node_to_destination, weight="length") <= maxDistance:
            route = route + route_next
            distance = distance + dist
            lastPoint= closest_node_to_placesToVisit[j]
            lastPointNum = j
            print("Trasa z węzła start do", j)
            break
    if j == len(placesToVisit)-1:
        print("Nie można odwiedzić miejsc dodatkowych w zadanym dystansie. Trasa tylko od punktu startowego do docelowego")
        finished = True
        #tu ustawienie jakiejś flagi, żeby było wiadomo, czy wykonywać pętle, wstępnie ma być przypisane start-dest
        route = nx.shortest_path(G, closest_node_to_start, closest_node_to_destination)
        distance = nx.shortest_path_length(G, closest_node_to_start, closest_node_to_destination, weight="length")


iter = lastPointNum
#p k p-wezel ostatni na trasie, k wezel mozliwy kolejny
if finished is False:
    for i in range(iter, len(placesToVisit), 1):

        startIter= lastPointNum + 1 #rozpoczynamy przegladanie od kolejnego wezle (nie cofamy sie juz)
        for k in range(startIter, len(placesToVisit),1):
            try:
                route_next = nx.shortest_path(G, closest_node_to_placesToVisit[lastPointNum], closest_node_to_placesToVisit[k])
            except nx.NetworkXNoPath:
                continue
            dist = nx.shortest_path_length(G, closest_node_to_placesToVisit[lastPointNum], closest_node_to_placesToVisit[k],weight="length")
            if (distance + dist) < maxDistance:
                if (distance + dist + nx.shortest_path_length(G, closest_node_to_placesToVisit[k],closest_node_to_destination, weight="length")) <= maxDistance:
                    distance = distance + dist
                    del route_next[0]
                    route = route + route_next
                    lastPointNum = k
            if k == len(placesToVisit)-1:
                #polaczenie z destination
                route_next = nx.shortest_path(G, closest_node_to_placesToVisit[lastPointNum],closest_node_to_destination)
                dist = nx.shortest_path_length(G, closest_node_to_placesToVisit[lastPointNum],closest_node_to_destination, weight="length")
                del route_next[0]
                route = route + route_next
                distance = distance + dist
                print("Sumaryczny dystans: ",distance)
                finished = True
                break
        if finished is True:
            break


print("Route",route)
print("Sumaryczny dystans:", distance)


m = ox.plot_route_folium(G, route, route_color='green')
folium.Marker(location=start, icon=folium.Icon(color='red')).add_to(m)
folium.Marker(location=destination, icon=folium.Icon(color='green')).add_to(m)

for i in range(len(placesToVisit)):
    folium.Marker(location=placesToVisit[i], icon=folium.Icon(color='blue')).add_to(m)


filepath = 'route_graph.html'
m.save(filepath)
#IFrame(m, width=600, height=500)
